package sda.ex.machines.wallets;

import sda.ex.machines.commons.Coins;
import sda.ex.machines.wallets.exceptions.WalletRestPreparingException;

import java.util.Map;

public interface IWallet {
    Map<Coins, Integer> getRest(int amount, int productPrice) throws WalletRestPreparingException;
}
