package sda.ex.machines;

import sda.ex.machines.commons.Coins;
import sda.ex.machines.containers.IContainer;
import sda.ex.machines.containers.VendingMachineContainer;
import sda.ex.machines.wallets.IWallet;
import sda.ex.machines.wallets.VendingMachineWallet;
import sda.ex.machines.wallets.exceptions.WalletRestPreparingException;

import java.util.Map;

public class VendingMachine implements IMachine {

    private String choice;

    private IContainer container;

    private IWallet wallet;

    private int money = 0;

    public VendingMachine() {
        container = new VendingMachineContainer();
        wallet = new VendingMachineWallet();
    }

    @Override
    public void readChoice(String choice) {
        this.choice = choice;
    }

    @Override
    public IContainer getContainer() {
        return container;
    }

    @Override
    public int getPaymentAmount() {
        return container.getDispenserByCode(choice).getProduct().getPrice();
    }

    @Override
    public void insertCoin(Coins coin) {
        this.money += coin.getCents();
    }

    @Override
    public boolean isEnough() {
        return this.money >= getPaymentAmount();
    }

    @Override
    public Map<Coins, Integer> prepareRest() throws WalletRestPreparingException {
        return wallet.getRest(money, getPaymentAmount());
    }
}
