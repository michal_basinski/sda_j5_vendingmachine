package sda.ex.machines;

import sda.ex.machines.commons.Coins;
import sda.ex.machines.containers.IContainer;
import sda.ex.machines.wallets.exceptions.WalletRestPreparingException;

import java.util.Map;

public interface IMachine {
    void readChoice(String choice);

    IContainer getContainer();

    int getPaymentAmount();

    void insertCoin(Coins coin);

    boolean isEnough();

    Map<Coins, Integer> prepareRest() throws WalletRestPreparingException;
}
