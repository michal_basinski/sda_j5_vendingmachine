package sda.ex.machines.containers;

import sda.ex.machines.dispensers.Dispenser;

import java.util.Set;

public interface IContainer {

    Dispenser getDispenserByCode(String code);

    Set<String> getCodes();
}
