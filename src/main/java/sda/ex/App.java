package sda.ex;

import sda.ex.machines.IMachine;
import sda.ex.machines.VendingMachine;
import sda.ex.machines.commons.Coins;
import sda.ex.machines.wallets.exceptions.WalletRestPreparingException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

public class App {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        IMachine vendingMachine = new VendingMachine();

        //==========WYPISANIE LISTY PRODUKTÓW I KODÓW========================
        Set<String> choicesMap = vendingMachine.getContainer().getCodes();

        for (String key : choicesMap) {
            System.out.println(key);
        }
        //====================================================================


        //=======================WYBÓR PRODUKTU===============================
        System.out.println("Wybierz produkt");
        String choice = reader.readLine();
        vendingMachine.readChoice(choice);
        int productPrice = vendingMachine.getPaymentAmount();
        //====================================================================


        try {
            //================ZAPŁATA ZA TOWAR====================================
            System.out.println("Kwota do zapłaty: " + formatNumber(productPrice));
            System.out.println("Zapłać za towar");
            boolean paid = false;
            while (paid == false) {
                String cents = reader.readLine();
                vendingMachine.insertCoin(identifyCoinByCentValue(cents));
                paid = vendingMachine.isEnough();
            }
            //=====================================================================

            //================WYDANIE RESZTY======================================
            Map<Coins, Integer> rest = vendingMachine.prepareRest();;

            System.out.println("Wydano resztę:");
            for (Coins key : rest.keySet()) {
                System.out.println(rest.get(key) + " x " + key.name());
            }
            //====================================================================
            System.out.println("Wydano produkt");
        } catch (WalletRestPreparingException e) {
            System.out.println("Proces zakupu przerwany - wydanie reszty niemożliwe. Zwrócono wpłacone pieniądze");
        }
    }

    private static Coins identifyCoinByCentValue(String coin) {
        Coins result;
        switch (coin) {
            case "1":
                result = Coins.ONE_GR;
                break;
            case "2":
                result = Coins.TWO_GR;
                break;
            case "5":
                result = Coins.FIVE_GR;
                break;
            case "10":
                result = Coins.TEN_GR;
                break;
            case "20":
                result = Coins.TWENTY_GR;
                break;
            case "50":
                result = Coins.FIFTY_GR;
                break;
            case "100":
                result = Coins.ONE_PLN;
                break;
            case "200":
                result = Coins.TWO_PLN;
                break;
            case "500":
                result = Coins.FIVE_PLN;
                break;
            case "1000":
                result = Coins.TEN_PLN;
                break;
            case "2000":
                result = Coins.TWENTY_PLN;
                break;
            case "5000":
                result = Coins.FIFTY_PLN;
                break;
            case "10000":
                result = Coins.ONE_HUNDRED_PLN;
                break;
            case "20000":
                result = Coins.TWO_HUNDRED_PLN;
                break;
            default:
                result = null;
                break;
        }
        return result;
    }

    private static float formatNumber(int number) {
        return (float) number / 100;
    }
}
